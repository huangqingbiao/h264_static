#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <getopt.h>
#include <cstring>
#include <cassert>
#include <csignal>
#include "h264_cap.h"

typedef struct
{
    char device[0x20];      //-d=eth0
    char dump_file[0x100];  //-f=eth0.pcap
    unsigned int limit;           //-c=1000
    unsigned int clock_rate;
}cmd_args_t;

cmd_args_t g_cmd_args;

void cmd_parse(cmd_args_t* cmd_args, int argc, char** argv)
{
    int c;

    *cmd_args->device = 0; //realtime needed
    strcpy(cmd_args->dump_file, "default.pcap"); //default
    cmd_args->limit = -1; //default
    cmd_args->clock_rate = 90000; //default

    while((c = getopt(argc, argv, "f:d:c:r:")) != -1)
    {
        switch(c)
        {
            case 'f':
                strncpy(cmd_args->dump_file, optarg, sizeof(cmd_args->dump_file));
                break;
            case 'd':
                strncpy(cmd_args->device, optarg, sizeof(cmd_args->device));
                break;
            case 'c':
                cmd_args->limit = (unsigned int)atol(optarg);
                break;
            case 'r':
                cmd_args->clock_rate = (unsigned int)atol(optarg);
            default:
                printf("invalid arg option(-%c)\n", c);
                exit(0);
        }
    }


    if(*cmd_args->device)
    {
        assert(*cmd_args->dump_file);
        h264_cap(cmd_args->device, cmd_args->limit, cmd_args->dump_file);
        h264_statistic(cmd_args->dump_file, cmd_args->clock_rate);
    } else if(*cmd_args->dump_file)
    {
        h264_statistic(cmd_args->dump_file, cmd_args->clock_rate);
    }
}


int main(int argc, char** argv) {
    cmd_parse(&g_cmd_args, argc, argv);
    return 0;
}