//
// Created by root on 11/29/17.
//

#ifndef UNTITLED_DEFAULT_CAP_H
#define UNTITLED_DEFAULT_CAP_H

#include <cstddef>
#include <cmath>
#include <map>
#include <pcap.h>
#define LOG(...) printf(__VA_ARGS__)

/*
*   应用层数据统计
*/
typedef struct
{
    void* base;
    int(*loop)(void* base, const u_char* data, unsigned int len, double arrival_time, unsigned int clock_rate);
}basic_cap_loop_callback_t;

/*
*   数据采集
*/
typedef struct
{
    char device[0x20];
    char dump_file[0x20];
    char filter[0x20];
    char err_buf[PCAP_ERRBUF_SIZE];
    pcap_t* handle;
    pcap_dumper_t* dumper;
    char finished;
    char offline;
}basic_cap_t;
/*
*     数据采集初始化(实时采集)
*     @param device 网络设备
*     @param dump_file pcap文件存放位置
*     @param filter 非应用层数据过滤
*     @return
*     @see
*     @note
 */
void basic_cap_init(basic_cap_t* cap, const char* device, const char* dump_file, const char* filter);
/*
*     数据采集初始化(从本地pcap文件)
*     @param dump_file pcap文件存放位置
*     @param filter 非应用层数据过滤
*     @return
*     @see
*     @note
 */
void basic_cap_init(basic_cap_t* cap, const char* dump_file, const char* filter);
/*
*   数据采集loop
*/
void basic_cap_loop(basic_cap_t* cap, basic_cap_loop_callback_t* callback, unsigned int clock_rate);
/*
*   数据采集釋放资源
*/
void basic_cap_term(basic_cap_t* cap);

#endif //UNTITLED_DEFAULT_CAP_H
