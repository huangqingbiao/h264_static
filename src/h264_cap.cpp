//
// Created by root on 11/30/17.
//

#include <netinet/in.h>
#include <cstdarg>
#include <cassert>
#include <set>
#include <cstdlib>
#include "h264_cap.h"
#include "basic_cap.h"
/*
*   rtp网络质量信息分组信息
*/
typedef struct
{
    unsigned int src_addr;
    unsigned short src_port;
    unsigned int des_addr;
    unsigned short des_port;
    unsigned short ssrc;
    unsigned char playload;
}rtp_stat_key;
/*
*   rtp网络质量信息输出排序
*/
typedef struct
{
    bool operator()(const rtp_stat_key& l, const rtp_stat_key& r)
    {
        if(l.src_addr != r.src_addr)    return l.src_addr < r.src_addr;
        if(l.src_port !=  r.src_port)   return l.src_port < r.src_port;
        if(l.des_addr !=  r.des_addr)   return l.des_addr < r.des_addr;
        if(l.des_port !=  r.des_port)   return l.des_port < r.des_port;
        if(l.ssrc !=  r.ssrc)           return l.ssrc     < r.ssrc;
        if(l.playload != r.playload)    return l.playload < r.playload;
        return false;
    }
}rtp_stat_key_cmp;
/*
 *  rtp网络质量详细信息
 */
typedef struct {
    unsigned int packets; 			//已采包数量
    unsigned int miss_packets; 		//丢失包数量
    double max_delta; 				//最大延迟 unit ms
    double max_fitter;				//最大抖动
    double mean_fitter;				//平均抖动

    unsigned int clock_rate;		//采集频率
    unsigned int prev_timestamp;	//上一次rtp包时间
    double prev_arrival_time;		//上一次rtp包接收时间
    unsigned int current_timestamp;	//当前rtp包时间
    double current_arrival_time;	//当前rtp包接收时间
    double current_fitter;			//当前抖动
    unsigned int first_packet;		//是否为第一个包
    std::set<unsigned short> seq_nr_set;	//rtp包次序集
}rtp_stat_detail;
// rtp 质量统计集
typedef std::map<rtp_stat_key, rtp_stat_detail, rtp_stat_key_cmp> rtp_stats;

/*
*     rtp_stat_detail初始化
*     @param timestamp rtp时间
*     @param arrival_time rtp包到达时间
*     @param seq_nr rtp次序
*     @param clock_rate 采集频率
*     @return
*     @see
*     @note
 */
void rtp_stat_init(rtp_stat_detail* stat, unsigned int timestamp, double arrival_time, unsigned short seq_nr, unsigned int clock_rate)
{
    stat->packets = 0;
    stat->max_delta = 0;
    stat->max_fitter = 0;
    stat->mean_fitter = 0;
    stat->current_fitter = 0;
    stat->current_arrival_time = arrival_time;
    stat->current_timestamp = timestamp;
    //stat->seq_nr_set.emplace(seq_nr);
    stat->seq_nr_set.insert(seq_nr);
    stat->clock_rate = clock_rate;
    stat->first_packet = 0;
}
/*
*     rtp_stat_detail 计算抖动延迟
*     @return
*     @see
*     @note
 */
void rtp_stat_gen(rtp_stat_detail* stat)
{
    if(stat->first_packet)
        return;

    double diff;
    if(stat->current_timestamp > stat->prev_timestamp)
        diff = fabs(1000.0 * (stat->current_timestamp - stat->prev_timestamp) / stat->clock_rate
                    - (stat->current_arrival_time - stat->prev_arrival_time));
    else
        diff = fabs(1000.0 * (stat->prev_timestamp - stat->current_timestamp) / stat->clock_rate
                    - (stat->current_arrival_time - stat->prev_arrival_time));

    if(diff >=  10000)
    {

        printf("pos=%u diff=%.3f cur_t=%u prev_t=%u cur_arr=%.3f prev_arr=%.3f\n",
               stat->seq_nr_set.size(),
               diff,
               stat->current_timestamp,
               stat->prev_timestamp,
               stat->current_arrival_time,
               stat->prev_arrival_time);
        printf("t_interval=%.3fms a_interval=%.3fms\n",
               1000.0 * (stat->current_timestamp - stat->prev_timestamp) / stat->clock_rate,
               (stat->current_arrival_time - stat->prev_arrival_time)
        );
    }
    stat->current_fitter = (diff + 15 * stat->current_fitter) / 16;

    if(stat->current_fitter > stat->max_fitter)
        stat->max_fitter = stat->current_fitter;
    double delta = stat->current_arrival_time - stat->prev_arrival_time;
    if(delta > stat->max_delta)
        stat->max_delta = delta;
    stat->mean_fitter = (stat->mean_fitter * (stat->packets - 1) + diff) / (stat->packets);
}
/*
*     rtp_stat_detail 时间更新
*     @param arrival_time rtp包到达时间
*     @param timestamp rtp时间
*     @return
*     @see
*     @note
 */
void rtp_stat_update(rtp_stat_detail* stat, double arrival_time, unsigned int timestamp)
{
    stat->prev_timestamp = stat->current_timestamp;
    stat->prev_arrival_time = stat->current_arrival_time;
    stat->current_timestamp = timestamp;
    stat->current_arrival_time = arrival_time;
}

/*
*     rtp_stats 检查包丢失
*     @return
*     @see
*     @note
 */
void rtp_stats_check_miss(rtp_stats* stats)
{
    for(rtp_stats::const_iterator n = stats->begin(); n != stats->end(); ++n)
    {
        rtp_stat_detail* stat = const_cast<rtp_stat_detail*>(&n->second);
        unsigned short prev_seq_nr;
        stat->miss_packets = 0;
        prev_seq_nr = *stat->seq_nr_set.begin();
        std::set<unsigned short>::const_iterator m = stat->seq_nr_set.begin();
        ++m;
        for(; m != stat->seq_nr_set.end(); ++m)
        {
            for(unsigned short pos = prev_seq_nr + 1; pos < *m; ++pos)
            {
                ++stat->miss_packets;
            }
            prev_seq_nr = *m;
        }
    }
}

/*
*     rtp_stats 显示统计信息
*     @return
*     @see
*     @note
 */
void rtp_stats_print(rtp_stats* stats)
{
    char src_host[0x20], des_host[0x20];
    for(rtp_stats::const_iterator m = stats->begin(); m != stats->end(); ++m)
    {
        snprintf(src_host, sizeof(src_host), "%d.%d.%d.%d:%d",
                 *((unsigned char*)&m->first.src_addr + 3),
                 *((unsigned char*)&m->first.src_addr + 2),
                 *((unsigned char*)&m->first.src_addr + 1),
                 *((unsigned char*)&m->first.src_addr + 0),
                 m->first.src_port
        );
        snprintf(des_host, sizeof(des_host), "%d.%d.%d.%d:%d",
                 *((unsigned char*)&m->first.des_addr + 3),
                 *((unsigned char*)&m->first.des_addr + 2),
                 *((unsigned char*)&m->first.des_addr + 1),
                 *((unsigned char*)&m->first.des_addr + 0),
                 m->first.des_port
        );
        printf("src:%s, des:%s, ssrc:%d payload:%d packet=%d, miss=%.3lf%%, max delta=%.3lf(ms), max fitter=%.3lf, mean fitter=%.3lf\n",
               src_host,
               des_host,
               m->first.ssrc,
               m->first.playload,
               m->second.packets,
               m->second.miss_packets * 100.0 / (m->second.packets + m->second.miss_packets), m->second.max_delta,
               m->second.max_fitter,
               m->second.mean_fitter
               );
    }
}

/*
*     progress_print 显示进度
*     @return
*     @see
*     @note
 */
unsigned short progress_print(unsigned short last_char_count, const char* fmt, ...)
{
    char buf[0x100];
    unsigned short ret;
    va_list va;
    va_start(va, fmt);
    for(;last_char_count--;)
        printf("\b");
    ret = vprintf(fmt, va);
    va_end(va);
    return ret;
}
/*
*	h264数据采集
*/
typedef struct
{
    basic_cap_t* cap;
    unsigned int max_packets;
    unsigned int current_packets;
    unsigned short progress_char_count;
}h264_cap_loop_t;

/*
*	h264_cap_loop_t h264数据采集进度显示
*/
void h264_cap_progress_print(h264_cap_loop_t* loop)
{
    if(loop->max_packets != (unsigned int)-1)
        loop->progress_char_count = progress_print(
                loop->progress_char_count,
                "h264_cap_progress cap packets:%8d/%-8d",
                loop->current_packets,
                loop->max_packets
        );
    else
        loop->progress_char_count = progress_print(
                loop->progress_char_count,
                "h264_cap_progress cap packets:%8d",
                loop->current_packets
        );
}

/*
*     h264_cap_loop_callback h264数据采集callback
*     @param base h264_cap_loop_t
*     @param data 数据内容
*     @param len 数据长度
*     @param arrival_time 到达时间
*     @return 0 not ok, -1 break loop, 1 ok
*     @see
*     @note
 */
int h264_cap_loop_callback(void* base, const u_char* data, unsigned int len, double arrival_time, unsigned int clock_rate)
{

    //ethernet ii + ip + udp 42
    //rtp 16
    //rtcp 8
    h264_cap_loop_t* h264_cap_loop = (h264_cap_loop_t*)base;

    if(len < 50)
        return 0;

    u_char uc = *(data + 43);
    if((uc & 0x7f) >= 0x60) //rtp h264 or rtp pt 0xc8-0xcc
    {
        if(h264_cap_loop && !h264_cap_loop->cap->offline)
        {
            ++h264_cap_loop->current_packets;
            h264_cap_progress_print(h264_cap_loop);
            if(h264_cap_loop->current_packets == h264_cap_loop->max_packets)
            {
                printf("\n");
                return -1;
            }
        }
        return 1;
    }
    if(uc >= 0xc8 && uc <= 0xcc)
        return 1;
    return 0;

}

/*
*     h264_cap_loop_init h264数据采集callback信息初始化
*     @param max_packets 最大数量
*     @return
*     @see
*     @note
 */
void h264_cap_loop_init(basic_cap_loop_callback_t* callback, basic_cap_t* cap, unsigned int max_packets)
{
    h264_cap_loop_t* h264_cap_loop = (h264_cap_loop_t*)malloc(sizeof(h264_cap_loop_t));
    assert(h264_cap_loop);
    h264_cap_loop->cap = cap;
    h264_cap_loop->max_packets = max_packets;
    h264_cap_loop->current_packets = 0;
    h264_cap_loop->progress_char_count = 0;
    callback->base = h264_cap_loop;
    callback->loop = h264_cap_loop_callback;
}
/*
*     h264_cap_loop_term h264数据采集callback信息释放
*     @param max_packets 最大数量
*     @return
*     @see
*     @note
 */
void h264_cap_loop_term(basic_cap_loop_callback_t* callback)
{
    free(callback->base);
}

/*
*     h264_statistic_loop_callback h264数据统计callback
*     @param base h264_cap_loop_t
*     @param data 数据内容
*     @param len 数据长度
*     @param arrival_time 到达时间
*     @return 0 
*     @see
*     @note
 */
int h264_statistic_loop_callback(void* base, const u_char* data, unsigned int len, double arrival_time, unsigned int clock_rate)
{

    rtp_stat_key key;
    rtp_stat_detail part;
    unsigned int timestamp;
    unsigned short seq_nr;

    rtp_stats* stats;
    u_char payload = *(data + 43);
    payload &= 0x7f;
    stats = (rtp_stats*)base;
    if(len < 50 || payload < 0x60)
        return 0;

    timestamp = ntohl(*(unsigned int*)(data + 46));
    seq_nr = ntohs(*(unsigned short*)(data + 44));
    key.src_addr = ntohl(*(unsigned int*)(data + 26));
    key.src_port = ntohs(*(unsigned short*)(data + 34));
    key.des_addr = ntohl(*(unsigned int*)(data + 30));
    key.des_port = ntohs(*(unsigned short*)(data + 36));
    key.ssrc = ntohs(*(unsigned short*)(data + 50));
    key.playload = payload;

    rtp_stats::const_iterator m = ((rtp_stats*)stats)->find(key);
    if (m == ((rtp_stats*)stats)->end())
    {
        rtp_stat_init(&part, timestamp, arrival_time, seq_nr, clock_rate);
        ++part.packets;
        //((rtp_stats*)stats)->emplace(key, part);
        ((rtp_stats*)stats)->insert(std::pair<rtp_stat_key, rtp_stat_detail>(key, part));
    } else {
        //(const_cast<std::set<unsigned short>*>(&m->second.seq_nr_set))->emplace(seq_nr);
        (const_cast<std::set<unsigned short>*>(&m->second.seq_nr_set))->insert(seq_nr);
        ++const_cast<rtp_stat_detail*>(&m->second)->packets;
        rtp_stat_update(const_cast<rtp_stat_detail *>(&m->second), arrival_time, timestamp);
        rtp_stat_gen(const_cast<rtp_stat_detail *>(&m->second));
    }
    return 0;
}

/*
*     h264_statistic_loop_init h264数据统计callback信息初始化
*     @return
*     @see
*     @note
 */
void h264_statistic_loop_init(basic_cap_loop_callback_t* callback)
{
    rtp_stats* stats = new rtp_stats;
    assert(stats);
    callback->base = stats;
    callback->loop = h264_statistic_loop_callback;
}

/*
*     h264_statistic_loop_init h264数据统计callback信息初始化
*     @return
*     @see
*     @note
 */
void h264_statistic_loop_term(basic_cap_loop_callback_t* callback)
{
    rtp_stats_check_miss((rtp_stats*)callback->base);
    rtp_stats_print((rtp_stats*)callback->base);
    delete((rtp_stats*)callback->base);
}



void h264_cap(const char* device, size_t limit, const char* dump_file)
{
    basic_cap_t cap;
    basic_cap_loop_callback_t loop;
    h264_cap_loop_init(&loop, &cap, limit);
    basic_cap_init(&cap, device, dump_file, "udp");
    basic_cap_loop(&cap, &loop, 0);
    basic_cap_term(&cap);
    h264_cap_loop_term(&loop);
}

void h264_statistic(const char* dump_file, unsigned int clock_rate)
{
    basic_cap_t cap;
    basic_cap_loop_callback_t loop;
    h264_statistic_loop_init(&loop);
    basic_cap_init(&cap, dump_file, "udp");
    basic_cap_loop(&cap, &loop, clock_rate);
    basic_cap_term(&cap);
    h264_statistic_loop_term(&loop);
}
