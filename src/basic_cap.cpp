//
// Created by root on 11/29/17.
//
#include <pcap.h>
#include <cstring>
#include <cassert>
#include <cstdarg>
#include <csignal>
#include "basic_cap.h"

basic_cap_t* g_cap;
void stop_signal_handle(int signo)
{
    if(g_cap && !g_cap->offline && !g_cap->finished)
    {
        pcap_breakloop(g_cap->handle);
        g_cap->finished = 1;
    }
}

/*
*     basic_cap_log_err 数据采集错误日志
*     @return
*     @see
*     @note
 */
void basic_cap_log_err(basic_cap_t* cap, const char* where)
{
    LOG("%s error:%s\n", where, cap->err_buf);
}
/*
*     basic_cap_log_err 数据采集日志
*     @return
*     @see
*     @note
 */
void basic_cap_log(basic_cap_t* cap, const char* where, const char* fmt, ...)
{
    char buf[PCAP_ERRBUF_SIZE];
    va_list va;
    va_start(va, fmt);
    vsnprintf(buf, sizeof(buf), fmt, va);
    LOG("%s:%s\n", where, buf);
}

int basic_cap_init(basic_cap_t* cap)
{
    bpf_u_int32  mask;
    bpf_u_int32  net;
    bpf_program fp;
    if(cap->offline)
    {
        if((cap->handle = pcap_open_offline(cap->dump_file, cap->err_buf)) == 0)
        {
            basic_cap_log_err(cap, "pcap_open_offline");
            return -1;
        }
    }
    else
    {
        if(pcap_lookupnet(cap->device, &net, &mask, cap->err_buf) == -1)
        {
            basic_cap_log_err(cap, "pcap_lookupnet");
            return -1;
        }

        if((cap->handle = pcap_open_live(cap->device, BUFSIZ, 1, 1000, cap->err_buf)) == 0)
        {
            basic_cap_log_err(cap, "pcap_open_live");
            return -1;
        }
        if(*cap->dump_file)
        {
            if((cap->dumper = pcap_dump_open(cap->handle, cap->dump_file)) == 0)
            {
                snprintf(cap->err_buf, sizeof(cap->err_buf), pcap_geterr(cap->handle));
                basic_cap_log_err(cap, "pcap_dump_open");
                pcap_close(cap->handle);
                return -1;
            }
        }
    }

    if(*cap->filter && (pcap_compile(cap->handle, &fp, cap->filter, 0, net) == -1 ||
       pcap_setfilter(cap->handle, &fp) == -1))
    {
        snprintf(cap->err_buf, sizeof(cap->err_buf), pcap_geterr(cap->handle));
        basic_cap_log_err(cap, "pcap_compile or pcap_setfilter");
        if(!cap->offline)
            pcap_dump_close(cap->dumper);
        pcap_close(cap->handle);
        return -1;
    }
    return 0;
}

void basic_cap_init(basic_cap_t* cap, const char* device, const char* dump_file, const char* filter)
{
    assert(device && sizeof(cap->device) > strlen(device));
    strcpy(cap->device, device);
    if(filter)
    {
        assert(sizeof(cap->filter) > strlen(filter));
        strcpy(cap->filter, filter);
    }
    else
    {
        *cap->filter = 0;
    }
    if(dump_file)
    {
        assert(sizeof(cap->dump_file) > strlen(dump_file));
        strcpy(cap->dump_file, dump_file);
    }
    else
    {
        *cap->dump_file = 0;
    }
    cap->offline = 0;
    cap->finished = 0;
    assert(basic_cap_init(cap)== 0);
    signal(SIGINT, stop_signal_handle);
    g_cap = cap;
    basic_cap_log(cap, "basic_cap_init", "device=%s dump_file=%s filter=%s ok", device, dump_file, filter);
}


void basic_cap_init(basic_cap_t* cap, const char* dump_file, const char* filter)
{
    assert(dump_file && sizeof(cap->dump_file) > strlen(dump_file));
    strcpy(cap->dump_file, dump_file);
    if(filter )
    {
        assert(sizeof(cap->filter) > strlen(filter));
        strcpy(cap->filter, filter);
    }
    cap->offline = 1;
    cap->finished = 1;
    cap->dumper = 0;
    assert(basic_cap_init(cap)== 0);
    basic_cap_log(cap, "basic_cap_init", "offline dump_file=%s filter=%s ok", dump_file, filter);
}


void basic_cap_loop_callback(u_char *arg, const struct pcap_pkthdr *header, const u_char *data)
{
    double arrival_time;
    unsigned int len;
    int ret;

    basic_cap_t* cap = (basic_cap_t*)(((void**)arg)[0]);
    basic_cap_loop_callback_t* loop = (basic_cap_loop_callback_t*)(((void**)arg)[1]);
    unsigned int clock_rate = *(unsigned int*)(((void**)arg)[2]);

    if(!loop)
        pcap_breakloop(cap->handle);

    if(cap->offline)
        len = header->len;
    else
        len = header->caplen;

    arrival_time = header->ts.tv_sec * 1000 + header->ts.tv_usec * 1.0 / 1000;
    ret = loop->loop(loop->base, data, len, arrival_time, clock_rate);//@return 0 not ok, -1 break loop, 1 ok
    if(ret == 1 && !cap->offline)
        pcap_dump((u_char*)cap->dumper, header, data);
    if(ret == -1)
    {
        pcap_breakloop(cap->handle);
        cap->finished = 1;
    }
}


void basic_cap_loop(basic_cap_t* cap, basic_cap_loop_callback_t* callback, unsigned int clock_rate)
{
    void* args[3];
    args[0] = cap;
    args[1] = callback;
    args[2] = &clock_rate;
    pcap_loop(cap->handle, -1, basic_cap_loop_callback, (u_char*)args);
}
void basic_cap_term(basic_cap_t* cap)
{
    if(cap->dumper)
        pcap_dump_close(cap->dumper);
    pcap_close(cap->handle);
    if(cap->offline)
        basic_cap_log(cap, "basic_cap_term", "offline dump_file=%s filter=%s ok", cap->dump_file, cap->filter);
    else
        basic_cap_log(cap, "basic_cap_term", "device=%s dump_file=%s filter=%s ok", cap->device, cap->dump_file, cap->filter);
    g_cap = 0;
}

