//
// Created by root on 11/30/17.
//

#ifndef UNTITLED_H264_CAP_H
#define UNTITLED_H264_CAP_H

/*
 *     采集网口device上h264数据，采次limit次，输出至dump_file,输出网络质量信息
 *     @param device 网口名称
 *     @param dump_file pcap导出文件(nullable)
 *     @return
 *     @see
 *     @note
 */
void h264_cap(const char* device, size_t limit, const char* dump_file);
/*
 *     读取本地pcap文件，输出网络质量信息
 *     @param dump_file pcap文件
 *     @param clock_rate 采样频率
 *     @return
 *     @see
 *     @note
 */
void h264_statistic(const char* dump_file, unsigned int clock_rate);

#endif //UNTITLED_H264_CAP_H
